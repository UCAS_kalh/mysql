-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: bamboo
-- Source Schemata: bamboo
-- Created: Tue Jun 30 14:39:41 2015
-- Workbench Version: 6.3.3
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema bamboo
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `bamboo` ;
CREATE SCHEMA IF NOT EXISTS `bamboo` ;

-- ----------------------------------------------------------------------------
-- Table bamboo.acl_entry
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`acl_entry` (
  `ID` BIGINT(20) NOT NULL,
  `ACL_OBJECT_IDENTITY` BIGINT(20) NULL DEFAULT NULL,
  `TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SID` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ACE_ORDER` INT(11) NULL DEFAULT NULL,
  `MASK` INT(11) NULL DEFAULT NULL,
  `GRANTING` BIT(1) NULL DEFAULT NULL,
  `AUDIT_SUCCESS` BIT(1) NULL DEFAULT NULL,
  `AUDIT_FAILURE` BIT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `FK2FB5F83D988CEFE9` (`ACL_OBJECT_IDENTITY` ASC),
  CONSTRAINT `FK2FB5F83D988CEFE9`
    FOREIGN KEY (`ACL_OBJECT_IDENTITY`)
    REFERENCES `bamboo`.`acl_object_identity` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.acl_object_identity
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`acl_object_identity` (
  `ID` BIGINT(20) NOT NULL,
  `OBJECT_ID_CLASS` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `OBJECT_ID_IDENTITY` BIGINT(20) NULL DEFAULT NULL,
  `PARENT_OBJECT` BIGINT(20) NULL DEFAULT NULL,
  `ENTRIES_INHERITING` BIT(1) NULL DEFAULT NULL,
  `OWNER_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `OWNER_SID` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `FK988CEFE974D9E474` (`PARENT_OBJECT` ASC),
  CONSTRAINT `FK988CEFE974D9E474`
    FOREIGN KEY (`PARENT_OBJECT`)
    REFERENCES `bamboo`.`acl_object_identity` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.agent_assignment
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`agent_assignment` (
  `ASSIGNMENT_ID` BIGINT(20) NOT NULL,
  `EXECUTOR_ID` BIGINT(20) NULL DEFAULT NULL,
  `EXECUTOR_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `EXECUTABLE_ID` BIGINT(20) NULL DEFAULT NULL,
  `EXECUTABLE_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ASSIGNMENT_ID`),
  INDEX `executor_id_idx` (`EXECUTOR_ID` ASC),
  INDEX `executable_type_idx` (`EXECUTABLE_TYPE` ASC),
  INDEX `executable_id_idx` (`EXECUTABLE_ID` ASC),
  INDEX `executor_type_idx` (`EXECUTOR_TYPE` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.agent_authentication
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`agent_authentication` (
  `AUTHENTICATION_ID` BIGINT(20) NOT NULL,
  `IP_ADDRESS` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `UUID` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `IS_APPROVED` BIT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`AUTHENTICATION_ID`),
  UNIQUE INDEX `UUID` (`UUID` ASC),
  INDEX `uuid_idx` (`UUID` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_38321b_custom_content_link
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_38321b_custom_content_link` (
  `CONTENT_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `LINK_LABEL` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `LINK_URL` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SEQUENCE` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  INDEX `index_ao_38321b_cus1828044926` (`CONTENT_KEY` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_6bb97d_task_build_result
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_6bb97d_task_build_result` (
  `BUILD_NUMBER` INT(11) NULL DEFAULT NULL,
  `BUILD_STATE` TINYINT(1) NULL DEFAULT NULL,
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `JOB_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLAN_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_6bb97d_task_entry
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_6bb97d_task_entry` (
  `FIRST_BUILD` INT(11) NULL DEFAULT NULL,
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `JOB_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `LAST_BUILD` INT(11) NULL DEFAULT NULL,
  `PATH` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLAN_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `TEXT` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `index_ao_6bb97d_tas86454262` (`TYPE` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_6bb97d_te2_tb
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_6bb97d_te2_tb` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `TASK_BUILD_RESULT_ID` INT(11) NULL DEFAULT NULL,
  `TASK_ENTRY_ID` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `index_ao_6bb97d_te2430158182` (`TASK_BUILD_RESULT_ID` ASC),
  INDEX `index_ao_6bb97d_te21842029286` (`TASK_ENTRY_ID` ASC),
  CONSTRAINT `fk_ao_6bb97d_te2_tb_task_build_result_id`
    FOREIGN KEY (`TASK_BUILD_RESULT_ID`)
    REFERENCES `bamboo`.`ao_6bb97d_task_build_result` (`ID`),
  CONSTRAINT `fk_ao_6bb97d_te2_tb_task_entry_id`
    FOREIGN KEY (`TASK_ENTRY_ID`)
    REFERENCES `bamboo`.`ao_6bb97d_task_entry` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_7a45fb_aotracking_entry
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_7a45fb_aotracking_entry` (
  `ACTIVE` TINYINT(1) NULL DEFAULT NULL,
  `PLAN_ID` BIGINT(20) NULL DEFAULT '0',
  `TRACKING_ID` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`TRACKING_ID`),
  INDEX `index_ao_7a45fb_aot979049305` (`ACTIVE` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1436
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_7a45fb_aotracking_result
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_7a45fb_aotracking_result` (
  `BUILD_NUMBER` INT(11) NULL DEFAULT '0',
  `LINKED_TRACKING_ENTRY_ID` INT(11) NULL DEFAULT NULL,
  `RESULT_ID` INT(11) NOT NULL AUTO_INCREMENT,
  `RESULT_SUMMARY_ID` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`RESULT_ID`),
  INDEX `index_ao_7a45fb_aot189814902` (`BUILD_NUMBER` ASC),
  INDEX `index_ao_7a45fb_aot57609990` (`RESULT_SUMMARY_ID` ASC),
  INDEX `index_ao_7a45fb_aot1332136998` (`LINKED_TRACKING_ENTRY_ID` ASC),
  CONSTRAINT `fk_ao_7a45fb_aotracking_result_linked_tracking_entry_id`
    FOREIGN KEY (`LINKED_TRACKING_ENTRY_ID`)
    REFERENCES `bamboo`.`ao_7a45fb_aotracking_entry` (`TRACKING_ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 5650
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_7a45fb_aotracking_user
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_7a45fb_aotracking_user` (
  `LINKED_TRACKING_ENTRY_ID` INT(11) NULL DEFAULT NULL,
  `USERNAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `USER_ID` INT(11) NOT NULL AUTO_INCREMENT,
  `USER_WHO_UPDATED` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  INDEX `index_ao_7a45fb_aot3191716` (`USERNAME` ASC),
  INDEX `index_ao_7a45fb_aot1861572280` (`LINKED_TRACKING_ENTRY_ID` ASC),
  CONSTRAINT `fk_ao_7a45fb_aotracking_user_linked_tracking_entry_id`
    FOREIGN KEY (`LINKED_TRACKING_ENTRY_ID`)
    REFERENCES `bamboo`.`ao_7a45fb_aotracking_entry` (`TRACKING_ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 1125
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_8095e5_account
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_8095e5_account` (
  `AWS_ACCESS_KEY_ID` VARCHAR(32) CHARACTER SET 'utf8' NOT NULL,
  `AWS_SECRET_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CLOUD_PROVIDER_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL DEFAULT 'aws',
  `ID` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `V1_ID` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `U_AO_8095E5_ACCOUNT_NAME` (`NAME` ASC),
  UNIQUE INDEX `U_AO_8095E5_ACCOUNT_V1_ID` (`V1_ID` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_8095e5_connector
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_8095e5_connector` (
  `AWS_ACCOUNTID` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `DURATION_SECONDS_MAX` INT(11) NULL DEFAULT NULL,
  `IAM_POLICY` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ID` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `SCOPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `TYPE` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `V1_ID` INT(11) NOT NULL DEFAULT '0',
  `EXTERNAL_ID` VARCHAR(96) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ROLE_ARN` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `U_AO_8095E5_CONNECTOR_NAME` (`NAME` ASC),
  UNIQUE INDEX `U_AO_8095E5_CONNECTOR_V1_ID` (`V1_ID` ASC),
  INDEX `index_ao_8095e5_con1915366627` (`AWS_ACCOUNTID` ASC),
  CONSTRAINT `fk_ao_8095e5_connector_aws_accountid`
    FOREIGN KEY (`AWS_ACCOUNTID`)
    REFERENCES `bamboo`.`ao_8095e5_account` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_8095e5_connector_group
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_8095e5_connector_group` (
  `AWS_CONNECTORID` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ID` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `index_ao_8095e5_con1608022787` (`AWS_CONNECTORID` ASC),
  CONSTRAINT `fk_ao_8095e5_connector_group_aws_connectorid`
    FOREIGN KEY (`AWS_CONNECTORID`)
    REFERENCES `bamboo`.`ao_8095e5_connector` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ao_f1b80e_branch_selection
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ao_f1b80e_branch_selection` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `SELECTED_BRANCH_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SELECTED_REPO_ID` BIGINT(20) NULL DEFAULT '0',
  `USER` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.artifact
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`artifact` (
  `ARTIFACT_ID` BIGINT(20) NOT NULL,
  `LABEL` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `ARTIFACT_SIZE` BIGINT(20) NULL DEFAULT NULL,
  `CHAIN_ARTIFACT` BIT(1) NOT NULL,
  `GLOBALLY_STORED` BIT(1) NOT NULL,
  `LINK_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLAN_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `BUILD_NUMBER` INT(11) NOT NULL,
  PRIMARY KEY (`ARTIFACT_ID`),
  INDEX `artifact_plan_key` (`PLAN_KEY` ASC, `BUILD_NUMBER` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.artifact_definition
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`artifact_definition` (
  `ARTIFACT_DEFINITION_ID` BIGINT(20) NOT NULL,
  `LABEL` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `SRC_DIRECTORY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `COPY_PATTERN` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CHAIN_ARTIFACT` BIT(1) NOT NULL,
  `PRODUCER_JOB_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`ARTIFACT_DEFINITION_ID`),
  INDEX `art_def_producer_idx` (`PRODUCER_JOB_ID` ASC),
  CONSTRAINT `FKF88809E0D324424A`
    FOREIGN KEY (`PRODUCER_JOB_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.build
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`build` (
  `BUILD_ID` BIGINT(20) NOT NULL,
  `BUILD_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  `FULL_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BUILDKEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `TITLE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `FIRST_BUILD_NUMBER` INT(11) NULL DEFAULT NULL,
  `LATEST_BUILD_NUMBER` INT(11) NULL DEFAULT NULL,
  `NEXT_BUILD_NUMBER` INT(11) NULL DEFAULT NULL,
  `SUSPENDED_FROM_BUILDING` BIT(1) NULL DEFAULT NULL,
  `MARKED_FOR_DELETION` BIT(1) NULL DEFAULT NULL,
  `PROJECT_ID` BIGINT(20) NOT NULL,
  `MASTER_ID` BIGINT(20) NULL DEFAULT NULL,
  `NOTIFICATION_SET` BIGINT(20) NULL DEFAULT NULL,
  `LINKED_JIRA_ISSUE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `REM_JIRA_LINK_REQUIRED` BIT(1) NULL DEFAULT NULL,
  `REQUIREMENT_SET` BIGINT(20) NULL DEFAULT NULL,
  `STAGE_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`BUILD_ID`),
  UNIQUE INDEX `FULL_KEY` (`FULL_KEY` ASC),
  UNIQUE INDEX `full_key_unique` (`FULL_KEY` ASC),
  INDEX `build_key_idx` (`FULL_KEY` ASC),
  INDEX `plan_deletion_idx` (`MARKED_FOR_DELETION` ASC),
  INDEX `FK3C9CE4E707D72EE` (`NOTIFICATION_SET` ASC),
  INDEX `FK3C9CE4EB2B11C18` (`MASTER_ID` ASC),
  INDEX `FK3C9CE4EA77F0821` (`PROJECT_ID` ASC),
  INDEX `FK3C9CE4E7C814E1C` (`STAGE_ID` ASC),
  INDEX `FK3C9CE4E645E1626` (`REQUIREMENT_SET` ASC),
  CONSTRAINT `FK3C9CE4E645E1626`
    FOREIGN KEY (`REQUIREMENT_SET`)
    REFERENCES `bamboo`.`requirement_set` (`REQUIREMENT_SET_ID`),
  CONSTRAINT `FK3C9CE4E707D72EE`
    FOREIGN KEY (`NOTIFICATION_SET`)
    REFERENCES `bamboo`.`notification_sets` (`NOTIFICATION_SET_ID`),
  CONSTRAINT `FK3C9CE4E7C814E1C`
    FOREIGN KEY (`STAGE_ID`)
    REFERENCES `bamboo`.`chain_stage` (`STAGE_ID`),
  CONSTRAINT `FK3C9CE4EA77F0821`
    FOREIGN KEY (`PROJECT_ID`)
    REFERENCES `bamboo`.`project` (`PROJECT_ID`),
  CONSTRAINT `FK3C9CE4EB2B11C18`
    FOREIGN KEY (`MASTER_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.artifact_subscription
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`artifact_subscription` (
  `ARTIFACT_SUBSCRIPTION_ID` BIGINT(20) NOT NULL,
  `ARTIFACT_DEFINITION_ID` BIGINT(20) NOT NULL,
  `CONSUMER_JOB_ID` BIGINT(20) NOT NULL,
  `DST_DIRECTORY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ARTIFACT_SUBSCRIPTION_ID`),
  INDEX `art_sub_definition_idx` (`ARTIFACT_DEFINITION_ID` ASC),
  INDEX `art_sub_consumer_job_idx` (`CONSUMER_JOB_ID` ASC),
  CONSTRAINT `FK4F23A6AAD2A9566`
    FOREIGN KEY (`CONSUMER_JOB_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`),
  CONSTRAINT `FK4F23A6ADEF697DA`
    FOREIGN KEY (`ARTIFACT_DEFINITION_ID`)
    REFERENCES `bamboo`.`artifact_definition` (`ARTIFACT_DEFINITION_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.audit_log
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`audit_log` (
  `AUDIT_ID` BIGINT(20) NOT NULL,
  `MSG_TIME_STAMP` BIGINT(20) NULL DEFAULT NULL,
  `MSG` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `OLD_VALUE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `NEW_VALUE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `MSG_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLAN_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `JOB_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `TASK_HEADER` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ENTITY_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `USER_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`AUDIT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.auth_attempt_info
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`auth_attempt_info` (
  `id` BIGINT(20) NOT NULL,
  `USER_NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `AUTH_COUNT` INT(11) NULL DEFAULT NULL,
  `LAST_AUTH_TIMESTAMP` DATE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `USER_NAME` (`USER_NAME` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.author
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`author` (
  `AUTHOR_ID` BIGINT(20) NOT NULL,
  `LINKED_USER_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `AUTHOR_EMAIL` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `AUTHOR_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`AUTHOR_ID`),
  UNIQUE INDEX `AUTHOR_NAME` (`AUTHOR_NAME` ASC),
  UNIQUE INDEX `author_author_name_key` (`AUTHOR_NAME` ASC),
  INDEX `author_name_idx` (`AUTHOR_NAME` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.bandana
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`bandana` (
  `BANDANA_ID` BIGINT(20) NOT NULL,
  `BUILD_ID` BIGINT(20) NULL DEFAULT NULL,
  `BANDANA_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SERIALIZED_DATA` MEDIUMTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`BANDANA_ID`),
  INDEX `band_key_idx` (`BUILD_ID` ASC, `BANDANA_KEY` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.branch_commit_info
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`branch_commit_info` (
  `BRANCH_COMMIT_INFO_ID` BIGINT(20) NOT NULL,
  `BRANCH_ID` BIGINT(20) NOT NULL,
  `CREATING_AUTHOR_ID` BIGINT(20) NULL DEFAULT NULL,
  `CREATING_COMMIT_DATE` DATETIME NULL DEFAULT NULL,
  `CREATING_CHANGE_SET_ID` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `LATEST_COMMIT_AUTHOR_ID` BIGINT(20) NULL DEFAULT NULL,
  `LATEST_COMMIT_DATE` DATETIME NULL DEFAULT NULL,
  `LATEST_COMMIT_CHANGE_SET_ID` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`BRANCH_COMMIT_INFO_ID`),
  UNIQUE INDEX `BRANCH_ID` (`BRANCH_ID` ASC),
  INDEX `branch_creating_authorId` (`CREATING_AUTHOR_ID` ASC),
  INDEX `branch_last_commit_authorId` (`LATEST_COMMIT_AUTHOR_ID` ASC),
  INDEX `branch_commit_branch_id_idx` (`BRANCH_ID` ASC),
  CONSTRAINT `FK13EAB7994E9C6E49`
    FOREIGN KEY (`CREATING_AUTHOR_ID`)
    REFERENCES `bamboo`.`author` (`AUTHOR_ID`),
  CONSTRAINT `FK13EAB799FFA424FF`
    FOREIGN KEY (`LATEST_COMMIT_AUTHOR_ID`)
    REFERENCES `bamboo`.`author` (`AUTHOR_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.brs_artifact_link
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`brs_artifact_link` (
  `ARTIFACT_LINK_ID` BIGINT(20) NOT NULL,
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NOT NULL,
  `PRODUCERJOBRESULT_ID` BIGINT(20) NULL DEFAULT NULL,
  `ARTIFACT_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`ARTIFACT_LINK_ID`),
  INDEX `producerJobResultId` (`PRODUCERJOBRESULT_ID` ASC),
  INDEX `buildResultSummaryId` (`BUILDRESULTSUMMARY_ID` ASC),
  INDEX `art_link_artifact_id` (`ARTIFACT_ID` ASC),
  CONSTRAINT `FKE1F6D10B3CF8CEA8`
    FOREIGN KEY (`ARTIFACT_ID`)
    REFERENCES `bamboo`.`artifact` (`ARTIFACT_ID`),
  CONSTRAINT `FKE1F6D10B6A657612`
    FOREIGN KEY (`PRODUCERJOBRESULT_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`),
  CONSTRAINT `FKE1F6D10BA958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.buildresultsummary
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`buildresultsummary` (
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NOT NULL,
  `BUILD_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  `BUILD_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BUILD_NUMBER` INT(11) NULL DEFAULT NULL,
  `BUILD_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `LIFE_CYCLE_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BUILD_DATE` DATETIME NULL DEFAULT NULL,
  `BUILD_CANCELLED_DATE` DATETIME NULL DEFAULT NULL,
  `BUILD_COMPLETED_DATE` DATETIME NULL DEFAULT NULL,
  `DURATION` BIGINT(20) NULL DEFAULT NULL,
  `PROCESSING_DURATION` BIGINT(20) NULL DEFAULT NULL,
  `TIME_TO_FIX` BIGINT(20) NULL DEFAULT NULL,
  `TRIGGER_REASON` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `DELTA_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BUILD_AGENT_ID` BIGINT(20) NULL DEFAULT NULL,
  `STAGERESULT_ID` BIGINT(20) NULL DEFAULT NULL,
  `RESTART_COUNT` INT(11) NULL DEFAULT NULL,
  `QUEUE_TIME` DATETIME NULL DEFAULT NULL,
  `MARKED_FOR_DELETION` BIT(1) NULL DEFAULT NULL,
  `ONCE_OFF` BIT(1) NULL DEFAULT NULL,
  `CUSTOM_BUILD` BIT(1) NULL DEFAULT NULL,
  `REBUILD` BIT(1) NULL DEFAULT NULL,
  `FAILURE_TEST_COUNT` INT(11) NULL DEFAULT NULL,
  `SUCCESS_TEST_COUNT` INT(11) NULL DEFAULT NULL,
  `TOTAL_TEST_COUNT` INT(11) NULL DEFAULT NULL,
  `BROKEN_TEST_COUNT` INT(11) NULL DEFAULT NULL,
  `EXISTING_TEST_COUNT` INT(11) NULL DEFAULT NULL,
  `FIXED_TEST_COUNT` INT(11) NULL DEFAULT NULL,
  `TOTAL_TEST_DURATION` BIGINT(20) NULL DEFAULT NULL,
  `QUARANTINED_TEST_COUNT` INT(11) NULL DEFAULT NULL,
  `VCS_UPDATE_TIME` DATETIME NULL DEFAULT NULL,
  `CHAIN_RESULT` BIGINT(20) NULL DEFAULT NULL,
  `CONTINUABLE` BIT(1) NULL DEFAULT NULL,
  `MERGERESULT_ID` BIGINT(20) NULL DEFAULT NULL,
  `VARIABLE_CONTEXT_BASELINE_ID` BIGINT(20) NULL DEFAULT NULL,
  `FIXED_IN_RESULT` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`BUILDRESULTSUMMARY_ID`),
  INDEX `key_number_delta_state` (`DELTA_STATE` ASC),
  INDEX `brs_build_state_idx` (`BUILD_STATE` ASC),
  INDEX `brs_life_cycle_state_idx` (`LIFE_CYCLE_STATE` ASC),
  INDEX `brs_deletion_idx` (`MARKED_FOR_DELETION` ASC),
  INDEX `brs_stage_result_id_idx` (`STAGERESULT_ID` ASC),
  INDEX `key_number_results_index` (`BUILD_KEY` ASC, `BUILD_NUMBER` ASC),
  INDEX `brs_agent_idx` (`BUILD_AGENT_ID` ASC),
  INDEX `FK26506D3BCCACF65` (`MERGERESULT_ID` ASC),
  INDEX `rs_ctx_baseline_idx` (`VARIABLE_CONTEXT_BASELINE_ID` ASC),
  INDEX `brs_chain_result_summary_idx` (`CHAIN_RESULT` ASC),
  CONSTRAINT `FK26506D3B9E6537B`
    FOREIGN KEY (`CHAIN_RESULT`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`),
  CONSTRAINT `FK26506D3BCCACF65`
    FOREIGN KEY (`MERGERESULT_ID`)
    REFERENCES `bamboo`.`merge_result` (`MERGERESULT_ID`),
  CONSTRAINT `FK26506D3BCEDEEF5F`
    FOREIGN KEY (`STAGERESULT_ID`)
    REFERENCES `bamboo`.`chain_stage_result` (`STAGERESULT_ID`),
  CONSTRAINT `FK26506D3BE3B5B062`
    FOREIGN KEY (`VARIABLE_CONTEXT_BASELINE_ID`)
    REFERENCES `bamboo`.`variable_context_baseline` (`VARIABLE_CONTEXT_BASELINE_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.brs_consumed_subscription
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`brs_consumed_subscription` (
  `CONSUMED_SUBSCRIPTION_ID` BIGINT(20) NOT NULL,
  `DST_DIRECTORY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CONSUMER_RESULTSUMMARY_ID` BIGINT(20) NOT NULL,
  `ARTIFACT_LINK_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`CONSUMED_SUBSCRIPTION_ID`),
  INDEX `consumerResultSummaryId` (`CONSUMER_RESULTSUMMARY_ID` ASC),
  INDEX `artifactLinkId` (`ARTIFACT_LINK_ID` ASC),
  CONSTRAINT `FKEC405ED86EAFB613`
    FOREIGN KEY (`ARTIFACT_LINK_ID`)
    REFERENCES `bamboo`.`brs_artifact_link` (`ARTIFACT_LINK_ID`),
  CONSTRAINT `FKEC405ED8D7E7D97A`
    FOREIGN KEY (`CONSUMER_RESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.brs_linkedjiraissues
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`brs_linkedjiraissues` (
  `LINKEDJIRAISSUE_ID` BIGINT(20) NOT NULL,
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NULL DEFAULT NULL,
  `JIRA_ISSUE_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `JIRA_ISSUE_LINK_TYPE` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`LINKEDJIRAISSUE_ID`),
  INDEX `brs_linkedjiraissues_brs_idx` (`BUILDRESULTSUMMARY_ID` ASC),
  CONSTRAINT `FK45B7017DA958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.requirement_set
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`requirement_set` (
  `REQUIREMENT_SET_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`REQUIREMENT_SET_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.notification_sets
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`notification_sets` (
  `NOTIFICATION_SET_ID` BIGINT(20) NOT NULL,
  `SET_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`NOTIFICATION_SET_ID`),
  INDEX `setTypeIndex` (`SET_TYPE` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.chain_stage
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`chain_stage` (
  `STAGE_ID` BIGINT(20) NOT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `OPTIONAL_STAGE` BIT(1) NULL DEFAULT NULL,
  `MARKED_FOR_DELETION` BIT(1) NULL DEFAULT NULL,
  `BUILD_ID` BIGINT(20) NOT NULL,
  `LIST_POSITION` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`STAGE_ID`),
  INDEX `stage_deletion_idx` (`MARKED_FOR_DELETION` ASC),
  INDEX `FKB613CFC0D96054AC` (`BUILD_ID` ASC),
  CONSTRAINT `FKB613CFC0D96054AC`
    FOREIGN KEY (`BUILD_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.project
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`project` (
  `PROJECT_ID` BIGINT(20) NOT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  `PROJECT_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `TITLE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `MARKED_FOR_DELETION` BIT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`PROJECT_ID`),
  INDEX `project_key_idx` (`PROJECT_KEY` ASC),
  INDEX `project_deletion_idx` (`MARKED_FOR_DELETION` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.build_definition
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`build_definition` (
  `BUILD_DEFINITION_ID` BIGINT(20) NOT NULL,
  `BUILD_DEFINITION_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  `XML_DEFINITION_DATA` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BUILD_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`BUILD_DEFINITION_ID`),
  UNIQUE INDEX `BUILD_ID` (`BUILD_ID` ASC),
  CONSTRAINT `FK611B4BE4D96054AC`
    FOREIGN KEY (`BUILD_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.merge_result
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`merge_result` (
  `MERGERESULT_ID` BIGINT(20) NOT NULL,
  `FAILURE_REASON` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `INTEGRATION_BRANCH_VCS_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `INTEGRATION_VCS_LOCATION_ID` BIGINT(20) NOT NULL,
  `INTEGRATION_BRANCH_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `MERGE_RESULT_VCS_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BRANCH_TARGET_VCS_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BRANCH_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `MERGE_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PUSH_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `INTEGRATION_STRATEGY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`MERGERESULT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.chain_stage_result
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`chain_stage_result` (
  `STAGERESULT_ID` BIGINT(20) NOT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `OPTIONAL_STAGE` BIT(1) NULL DEFAULT NULL,
  `PROCESSING_DURATION` BIGINT(20) NULL DEFAULT NULL,
  `CHAINRESULT_ID` BIGINT(20) NOT NULL,
  `LIST_POSITION` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`STAGERESULT_ID`),
  INDEX `csr_chain_result_idx` (`CHAINRESULT_ID` ASC),
  CONSTRAINT `FKC07B17C433C66FC`
    FOREIGN KEY (`CHAINRESULT_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.variable_context_baseline
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`variable_context_baseline` (
  `VARIABLE_CONTEXT_BASELINE_ID` BIGINT(20) NOT NULL,
  `HASH_STRING` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`VARIABLE_CONTEXT_BASELINE_ID`),
  INDEX `var_ctx_hash_string` (`HASH_STRING` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.buildresultsummary_customdata
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`buildresultsummary_customdata` (
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NOT NULL,
  `CUSTOM_INFO_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CUSTOM_INFO_VALUE` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`BUILDRESULTSUMMARY_ID`, `CUSTOM_INFO_KEY`),
  INDEX `brs_customdata_brsId_idx` (`BUILDRESULTSUMMARY_ID` ASC),
  CONSTRAINT `FK30932C1FA958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.buildresultsummary_label
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`buildresultsummary_label` (
  `ID` BIGINT(20) NOT NULL,
  `LABEL_ID` BIGINT(20) NOT NULL,
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NULL DEFAULT NULL,
  `BUILD_ID` BIGINT(20) NULL DEFAULT NULL,
  `PROJECT_ID` BIGINT(20) NULL DEFAULT NULL,
  `USER_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `label_brsId_idx` (`BUILDRESULTSUMMARY_ID` ASC),
  INDEX `FK9043ED0A77F0821` (`PROJECT_ID` ASC),
  INDEX `FK9043ED0D96054AC` (`BUILD_ID` ASC),
  INDEX `FK9043ED0B9DE8666` (`LABEL_ID` ASC),
  CONSTRAINT `FK9043ED0A77F0821`
    FOREIGN KEY (`PROJECT_ID`)
    REFERENCES `bamboo`.`project` (`PROJECT_ID`),
  CONSTRAINT `FK9043ED0A958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`),
  CONSTRAINT `FK9043ED0B9DE8666`
    FOREIGN KEY (`LABEL_ID`)
    REFERENCES `bamboo`.`label` (`LABEL_ID`),
  CONSTRAINT `FK9043ED0D96054AC`
    FOREIGN KEY (`BUILD_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.label
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`label` (
  `LABEL_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `NAMESPACE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`LABEL_ID`),
  INDEX `label_name_namespace_index` (`NAME` ASC, `NAMESPACE` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.capability
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`capability` (
  `CAPABILITY_ID` BIGINT(20) NOT NULL,
  `KEY_IDENTIFIER` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `VALUE` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CAPABILITY_SET` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`CAPABILITY_ID`),
  INDEX `capability_set_index` (`CAPABILITY_SET` ASC),
  INDEX `capability_key_index` (`KEY_IDENTIFIER` ASC),
  CONSTRAINT `FKEE341118A542349B`
    FOREIGN KEY (`CAPABILITY_SET`)
    REFERENCES `bamboo`.`capability_set` (`CAPABILITY_SET_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.capability_set
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`capability_set` (
  `CAPABILITY_SET_ID` BIGINT(20) NOT NULL,
  `CAPABILITY_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CAPABILITY_SCOPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`CAPABILITY_SET_ID`),
  INDEX `cap_scope_idx` (`CAPABILITY_SCOPE` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.commit_files
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`commit_files` (
  `COMMIT_ID` BIGINT(20) NOT NULL,
  `COMMIT_FILE_NAME` VARCHAR(1000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `COMMIT_FILE_REIVISION` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `LIST_POSITION` INT(11) NOT NULL,
  PRIMARY KEY (`COMMIT_ID`, `LIST_POSITION`),
  CONSTRAINT `FKA9983D4F4D373123`
    FOREIGN KEY (`COMMIT_ID`)
    REFERENCES `bamboo`.`user_commit` (`COMMIT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.user_commit
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`user_commit` (
  `COMMIT_ID` BIGINT(20) NOT NULL,
  `REPOSITORY_CHANGESET_ID` BIGINT(20) NULL DEFAULT NULL,
  `AUTHOR_ID` BIGINT(20) NULL DEFAULT NULL,
  `COMMIT_DATE` DATETIME NULL DEFAULT NULL,
  `COMMIT_REVISION` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `COMMIT_COMMENT_CLOB` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`COMMIT_ID`),
  INDEX `commit_authorId` (`AUTHOR_ID` ASC),
  INDEX `commit_repositoryChangesetId` (`REPOSITORY_CHANGESET_ID` ASC),
  CONSTRAINT `FKF8936C2BCBADFCBD`
    FOREIGN KEY (`REPOSITORY_CHANGESET_ID`)
    REFERENCES `bamboo`.`repository_changeset` (`REPOSITORY_CHANGESET_ID`),
  CONSTRAINT `FKF8936C2BFE0C684F`
    FOREIGN KEY (`AUTHOR_ID`)
    REFERENCES `bamboo`.`author` (`AUTHOR_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.credentials
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`credentials` (
  `CREDENTIALS_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `XML` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`CREDENTIALS_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.dep_version_planresultkeys
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`dep_version_planresultkeys` (
  `DEP_VERSION_PLANRESULTKEYS_ID` BIGINT(20) NOT NULL,
  `DEPLOYMENT_VERSION_ID` BIGINT(20) NULL DEFAULT NULL,
  `PLAN_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `BUILD_NUMBER` INT(11) NOT NULL,
  PRIMARY KEY (`DEP_VERSION_PLANRESULTKEYS_ID`),
  INDEX `dver_pk_plan_key` (`PLAN_KEY` ASC, `BUILD_NUMBER` ASC),
  INDEX `dver_pk_version_id` (`DEPLOYMENT_VERSION_ID` ASC),
  CONSTRAINT `FK939E88317BD9BBC`
    FOREIGN KEY (`DEPLOYMENT_VERSION_ID`)
    REFERENCES `bamboo`.`deployment_version` (`DEPLOYMENT_VERSION_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_version
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_version` (
  `DEPLOYMENT_VERSION_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `CREATOR_USERNAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLAN_BRANCH_NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `PROJECT_ID` BIGINT(20) NULL DEFAULT NULL,
  `VARIABLE_CONTEXT_BASELINE_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`DEPLOYMENT_VERSION_ID`),
  INDEX `dv_ctx_baseline_idx` (`VARIABLE_CONTEXT_BASELINE_ID` ASC),
  INDEX `dversion_name` (`NAME` ASC),
  INDEX `dversion_dep_id` (`PROJECT_ID` ASC),
  CONSTRAINT `FK1178613EA77F0821`
    FOREIGN KEY (`PROJECT_ID`)
    REFERENCES `bamboo`.`deployment_project` (`DEPLOYMENT_PROJECT_ID`),
  CONSTRAINT `FK1178613EE3B5B062`
    FOREIGN KEY (`VARIABLE_CONTEXT_BASELINE_ID`)
    REFERENCES `bamboo`.`variable_context_baseline` (`VARIABLE_CONTEXT_BASELINE_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_env_vcs_location
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_env_vcs_location` (
  `ENV_VCS_LOCATION_ID` BIGINT(20) NOT NULL,
  `ENVIRONMENT_ID` BIGINT(20) NULL DEFAULT NULL,
  `VCS_LOCATION_ID` BIGINT(20) NULL DEFAULT NULL,
  `LIST_POSITION` INT(11) NOT NULL,
  PRIMARY KEY (`ENV_VCS_LOCATION_ID`),
  INDEX `env_vcs_location_env_id` (`ENVIRONMENT_ID` ASC),
  INDEX `env_vcs_location_repo_id` (`VCS_LOCATION_ID` ASC),
  CONSTRAINT `FKCAE794FA1D68B66C`
    FOREIGN KEY (`VCS_LOCATION_ID`)
    REFERENCES `bamboo`.`vcs_location` (`VCS_LOCATION_ID`),
  CONSTRAINT `FKCAE794FAA248FEE7`
    FOREIGN KEY (`ENVIRONMENT_ID`)
    REFERENCES `bamboo`.`deployment_environment` (`ENVIRONMENT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.vcs_location
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`vcs_location` (
  `VCS_LOCATION_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLUGIN_KEY` VARCHAR(4000) CHARACTER SET 'utf8' NOT NULL,
  `XML_DEFINITION_DATA` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `MARKED_FOR_DELETION` BIT(1) NOT NULL,
  `IS_GLOBAL` BIT(1) NOT NULL,
  PRIMARY KEY (`VCS_LOCATION_ID`),
  INDEX `plan_vcs_location_is_global` (`IS_GLOBAL` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_environment
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_environment` (
  `ENVIRONMENT_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `XML_DEFINITION_DATA` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `TRIGGERS_XML_DATA` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CONFIGURATION_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PACKAGE_DEFINITION_ID` BIGINT(20) NULL DEFAULT NULL,
  `REQUIREMENT_SET` BIGINT(20) NULL DEFAULT NULL,
  `NOTIFICATION_SET` BIGINT(20) NULL DEFAULT NULL,
  `LIST_POSITION` INT(11) NOT NULL,
  PRIMARY KEY (`ENVIRONMENT_ID`),
  INDEX `FKC1B0CB79707D72EE` (`NOTIFICATION_SET` ASC),
  INDEX `FKC1B0CB79645E1626` (`REQUIREMENT_SET` ASC),
  INDEX `env_dep_project_id` (`PACKAGE_DEFINITION_ID` ASC),
  CONSTRAINT `FKC1B0CB79645E1626`
    FOREIGN KEY (`REQUIREMENT_SET`)
    REFERENCES `bamboo`.`requirement_set` (`REQUIREMENT_SET_ID`),
  CONSTRAINT `FKC1B0CB79707D72EE`
    FOREIGN KEY (`NOTIFICATION_SET`)
    REFERENCES `bamboo`.`notification_sets` (`NOTIFICATION_SET_ID`),
  CONSTRAINT `FKC1B0CB79B414BF2E`
    FOREIGN KEY (`PACKAGE_DEFINITION_ID`)
    REFERENCES `bamboo`.`deployment_project` (`DEPLOYMENT_PROJECT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_project
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_project` (
  `DEPLOYMENT_PROJECT_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLAN_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`DEPLOYMENT_PROJECT_ID`),
  INDEX `deployment_plan_key` (`PLAN_KEY` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_project_item
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_project_item` (
  `DEPLOYMENT_PROJECT_ITEM_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `DEPLOYMENT_PROJECT_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`DEPLOYMENT_PROJECT_ITEM_ID`),
  INDEX `dep_pr_item_dep_id` (`DEPLOYMENT_PROJECT_ID` ASC),
  CONSTRAINT `FK4CE273D3BA4939DB`
    FOREIGN KEY (`DEPLOYMENT_PROJECT_ID`)
    REFERENCES `bamboo`.`deployment_project` (`DEPLOYMENT_PROJECT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_project_item_ba
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_project_item_ba` (
  `BAM_ARTIFACT_ITEM_ID` BIGINT(20) NOT NULL,
  `ARTIFACT_DEFINITION_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`BAM_ARTIFACT_ITEM_ID`),
  INDEX `pkg_item_art_def_idx` (`ARTIFACT_DEFINITION_ID` ASC),
  CONSTRAINT `FK2486042BDEF697DA`
    FOREIGN KEY (`ARTIFACT_DEFINITION_ID`)
    REFERENCES `bamboo`.`artifact_definition` (`ARTIFACT_DEFINITION_ID`),
  CONSTRAINT `FK2486042BE12AE48B`
    FOREIGN KEY (`BAM_ARTIFACT_ITEM_ID`)
    REFERENCES `bamboo`.`deployment_project_item` (`DEPLOYMENT_PROJECT_ITEM_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_result
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_result` (
  `DEPLOYMENT_RESULT_ID` BIGINT(20) NOT NULL,
  `VERSION_ID` BIGINT(20) NULL DEFAULT NULL,
  `VERSION_NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `ENVIRONMENT_ID` BIGINT(20) NULL DEFAULT NULL,
  `DEPLOYMENT_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `LIFE_CYCLE_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `STARTED_DATE` DATETIME NULL DEFAULT NULL,
  `QUEUED_DATE` DATETIME NULL DEFAULT NULL,
  `EXECUTED_DATE` DATETIME NULL DEFAULT NULL,
  `FINISHED_DATE` DATETIME NULL DEFAULT NULL,
  `AGENT_ID` BIGINT(20) NULL DEFAULT NULL,
  `VARIABLE_CONTEXT_BASELINE_ID` BIGINT(20) NULL DEFAULT NULL,
  `TRIGGER_REASON` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`DEPLOYMENT_RESULT_ID`),
  INDEX `dr_build_state_idx` (`DEPLOYMENT_STATE` ASC),
  INDEX `dr_dep_ver_id` (`VERSION_ID` ASC),
  INDEX `dr_ctx_baseline_idx` (`VARIABLE_CONTEXT_BASELINE_ID` ASC),
  INDEX `dr_environment_id` (`ENVIRONMENT_ID` ASC),
  INDEX `dr_agent_idx` (`AGENT_ID` ASC),
  INDEX `dr_life_cycle_state_idx` (`LIFE_CYCLE_STATE` ASC),
  CONSTRAINT `FKB7ACDAD7A248FEE7`
    FOREIGN KEY (`ENVIRONMENT_ID`)
    REFERENCES `bamboo`.`deployment_environment` (`ENVIRONMENT_ID`),
  CONSTRAINT `FKB7ACDAD7E3B5B062`
    FOREIGN KEY (`VARIABLE_CONTEXT_BASELINE_ID`)
    REFERENCES `bamboo`.`variable_context_baseline` (`VARIABLE_CONTEXT_BASELINE_ID`),
  CONSTRAINT `FKB7ACDAD7F4F36A02`
    FOREIGN KEY (`VERSION_ID`)
    REFERENCES `bamboo`.`deployment_version` (`DEPLOYMENT_VERSION_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_result_customdata
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_result_customdata` (
  `DEPLOYMENT_RESULT_ID` BIGINT(20) NOT NULL,
  `CUSTOM_INFO_VALUE` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CUSTOM_INFO_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`DEPLOYMENT_RESULT_ID`, `CUSTOM_INFO_KEY`),
  CONSTRAINT `FKC16165037C500F83`
    FOREIGN KEY (`DEPLOYMENT_RESULT_ID`)
    REFERENCES `bamboo`.`deployment_result` (`DEPLOYMENT_RESULT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_variable_subs
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_variable_subs` (
  `VARIABLE_SUBSTITUTION_ID` BIGINT(20) NOT NULL,
  `VARIABLE_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `VARIABLE_VALUE` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `VARIABLE_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `DEPLOYMENT_RESULT_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`VARIABLE_SUBSTITUTION_ID`),
  INDEX `dep_var_subst_type_idx` (`VARIABLE_TYPE` ASC),
  INDEX `dep_var_subst_result_idx` (`DEPLOYMENT_RESULT_ID` ASC),
  CONSTRAINT `FKA2C9E21C7C500F83`
    FOREIGN KEY (`DEPLOYMENT_RESULT_ID`)
    REFERENCES `bamboo`.`deployment_result` (`DEPLOYMENT_RESULT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_version_changeset
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_version_changeset` (
  `DEP_VERSION_CHANGESET_ID` BIGINT(20) NOT NULL,
  `DEPLOYMENT_VERSION_ID` BIGINT(20) NOT NULL,
  `VCS_LOCATION_ID` BIGINT(20) NOT NULL,
  `CHANGESET_ID` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SKIPPED_COMMITS_COUNT` BIGINT(20) NOT NULL,
  PRIMARY KEY (`DEP_VERSION_CHANGESET_ID`),
  INDEX `dep_ver_vcs_chngset_vcsloc_idx` (`VCS_LOCATION_ID` ASC),
  INDEX `dep_ver_vcs_changeset_idx` (`DEPLOYMENT_VERSION_ID` ASC),
  CONSTRAINT `FKF6AD40D11D68B66C`
    FOREIGN KEY (`VCS_LOCATION_ID`)
    REFERENCES `bamboo`.`vcs_location` (`VCS_LOCATION_ID`),
  CONSTRAINT `FKF6AD40D17BD9BBC`
    FOREIGN KEY (`DEPLOYMENT_VERSION_ID`)
    REFERENCES `bamboo`.`deployment_version` (`DEPLOYMENT_VERSION_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_version_commit
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_version_commit` (
  `DEPLOYMENT_VERSION_COMMIT_ID` BIGINT(20) NOT NULL,
  `DEP_VERSION_CHANGESET_ID` BIGINT(20) NULL DEFAULT NULL,
  `AUTHOR_ID` BIGINT(20) NULL DEFAULT NULL,
  `COMMIT_DATE` DATETIME NULL DEFAULT NULL,
  `COMMIT_REVISION` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `COMMIT_COMMENT_CLOB` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`DEPLOYMENT_VERSION_COMMIT_ID`),
  INDEX `dep_ver_commit_changeset_id` (`DEP_VERSION_CHANGESET_ID` ASC),
  INDEX `dep_ver_commit_author_id` (`AUTHOR_ID` ASC),
  CONSTRAINT `FK6A5377581F560C5F`
    FOREIGN KEY (`DEP_VERSION_CHANGESET_ID`)
    REFERENCES `bamboo`.`deployment_version_changeset` (`DEP_VERSION_CHANGESET_ID`),
  CONSTRAINT `FK6A537758FE0C684F`
    FOREIGN KEY (`AUTHOR_ID`)
    REFERENCES `bamboo`.`author` (`AUTHOR_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_version_item
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_version_item` (
  `DEPLOYMENT_VERSION_ITEM_ID` BIGINT(20) NOT NULL,
  `DEPLOYMENT_VERSION_ID` BIGINT(20) NULL DEFAULT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`DEPLOYMENT_VERSION_ITEM_ID`),
  INDEX `dver_item_dversion` (`DEPLOYMENT_VERSION_ID` ASC),
  CONSTRAINT `FKEC5E1747BD9BBC`
    FOREIGN KEY (`DEPLOYMENT_VERSION_ID`)
    REFERENCES `bamboo`.`deployment_version` (`DEPLOYMENT_VERSION_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_version_item_ba
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_version_item_ba` (
  `VERSION_BAM_ARTIFACT_ITEM_ID` BIGINT(20) NOT NULL,
  `PLAN_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `BUILD_NUMBER` INT(11) NOT NULL,
  `LABEL` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `LOCATION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `COPY_PATTERN` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `ARTIFACT_SIZE` BIGINT(20) NULL DEFAULT NULL,
  `ARTIFACT_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`VERSION_BAM_ARTIFACT_ITEM_ID`),
  INDEX `ver_link_artifact_id` (`ARTIFACT_ID` ASC),
  CONSTRAINT `FK2598A6EA18C479B2`
    FOREIGN KEY (`VERSION_BAM_ARTIFACT_ITEM_ID`)
    REFERENCES `bamboo`.`deployment_version_item` (`DEPLOYMENT_VERSION_ITEM_ID`),
  CONSTRAINT `FK2598A6EA3CF8CEA8`
    FOREIGN KEY (`ARTIFACT_ID`)
    REFERENCES `bamboo`.`artifact` (`ARTIFACT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_version_jira_issue
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_version_jira_issue` (
  `DEP_VER_JIRA_ISSUE_ID` BIGINT(20) NOT NULL,
  `DEPLOYMENT_VERSION_ID` BIGINT(20) NULL DEFAULT NULL,
  `JIRA_ISSUE_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `JIRA_ISSUE_LINK_TYPE` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`DEP_VER_JIRA_ISSUE_ID`),
  INDEX `dep_ver_jiraissues_key_idx` (`JIRA_ISSUE_KEY` ASC),
  INDEX `dep_ver_jiraissues_verid_idx` (`DEPLOYMENT_VERSION_ID` ASC),
  CONSTRAINT `FK80B13A897BD9BBC`
    FOREIGN KEY (`DEPLOYMENT_VERSION_ID`)
    REFERENCES `bamboo`.`deployment_version` (`DEPLOYMENT_VERSION_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_version_naming
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_version_naming` (
  `VERSION_NAMING_SCHEME_ID` BIGINT(20) NOT NULL,
  `DEPLOYMENT_PROJECT_ID` BIGINT(20) NOT NULL,
  `NEXT_VERSION_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `AUTO_INCREMENT` BIT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`VERSION_NAMING_SCHEME_ID`),
  INDEX `ver_name_dep_proj_idx` (`DEPLOYMENT_PROJECT_ID` ASC),
  CONSTRAINT `FK7C536EA9BA4939DB`
    FOREIGN KEY (`DEPLOYMENT_PROJECT_ID`)
    REFERENCES `bamboo`.`deployment_project` (`DEPLOYMENT_PROJECT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.deployment_version_status
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`deployment_version_status` (
  `DEPLOYMENT_VERSION_STATUS_ID` BIGINT(20) NOT NULL,
  `VERSION_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `USER_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `DEPLOYMENT_VERSION_ID` BIGINT(20) NULL DEFAULT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`DEPLOYMENT_VERSION_STATUS_ID`),
  INDEX `vrs_status_idx` (`VERSION_STATE` ASC),
  INDEX `vrs_st_userName_idx` (`USER_NAME` ASC),
  INDEX `version_id_idx` (`DEPLOYMENT_VERSION_ID` ASC),
  CONSTRAINT `FK85E21E137BD9BBC`
    FOREIGN KEY (`DEPLOYMENT_VERSION_ID`)
    REFERENCES `bamboo`.`deployment_version` (`DEPLOYMENT_VERSION_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.ec2_image_script
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`ec2_image_script` (
  `ELASTIC_IMAGE_ID` BIGINT(20) NOT NULL,
  `SCRIPT_ID` BIGINT(20) NOT NULL,
  `SCRIPT_INDEX` INT(11) NOT NULL,
  PRIMARY KEY (`ELASTIC_IMAGE_ID`, `SCRIPT_INDEX`),
  INDEX `FK435AAF5AB4FE710F` (`SCRIPT_ID` ASC),
  CONSTRAINT `FK435AAF5A12C89CE9`
    FOREIGN KEY (`ELASTIC_IMAGE_ID`)
    REFERENCES `bamboo`.`elastic_image` (`ELASTIC_IMAGE_ID`),
  CONSTRAINT `FK435AAF5AB4FE710F`
    FOREIGN KEY (`SCRIPT_ID`)
    REFERENCES `bamboo`.`script` (`SCRIPT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.elastic_image
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`elastic_image` (
  `ELASTIC_IMAGE_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `AMI_IMAGE_ID` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `EBS_SNAPSHOT_ID` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `INSTANCE_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `AVAILABILITY_ZONE` VARCHAR(64) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SHIPPED_WITH_BAMBOO` BIT(1) NULL DEFAULT NULL,
  `IMAGE_DISABLED` BIT(1) NULL DEFAULT NULL,
  `PRODUCT` VARCHAR(32) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `REGION` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ROOT_DEVICE_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ARCHITECTURE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLATFORM` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SUBNET_ID` VARCHAR(32) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CAPABILITY_SET` BIGINT(20) NULL DEFAULT NULL,
  `IMAGE_FILES_VERSION` VARCHAR(16) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `OS_NAME` VARCHAR(64) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ELASTIC_IMAGE_ID`),
  INDEX `FK581FB731A542349B` (`CAPABILITY_SET` ASC),
  CONSTRAINT `FK581FB731A542349B`
    FOREIGN KEY (`CAPABILITY_SET`)
    REFERENCES `bamboo`.`capability_set` (`CAPABILITY_SET_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.script
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`script` (
  `SCRIPT_ID` BIGINT(20) NOT NULL,
  `body` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`SCRIPT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.elastic_schedule
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`elastic_schedule` (
  `SCHEDULE_ID` BIGINT(20) NOT NULL,
  `CRON_EXP` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `NUMBER_ACTIVE` INT(11) NULL DEFAULT NULL,
  `ENABLED` BIT(1) NULL DEFAULT NULL,
  `ELASTIC_IMAGE` BIGINT(20) NULL DEFAULT NULL,
  `COMPARISON` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`SCHEDULE_ID`),
  INDEX `FK1C6B30E1581FB731` (`ELASTIC_IMAGE` ASC),
  CONSTRAINT `FK1C6B30E1581FB731`
    FOREIGN KEY (`ELASTIC_IMAGE`)
    REFERENCES `bamboo`.`elastic_image` (`ELASTIC_IMAGE_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.external_entities
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`external_entities` (
  `id` BIGINT(20) NOT NULL,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `type` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.external_members
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`external_members` (
  `extentityid` BIGINT(20) NOT NULL,
  `groupid` BIGINT(20) NOT NULL,
  PRIMARY KEY (`groupid`, `extentityid`),
  INDEX `FKD8C8D8A5F25E5D5F` (`extentityid` ASC),
  CONSTRAINT `FKD8C8D8A5117D5FDA`
    FOREIGN KEY (`groupid`)
    REFERENCES `bamboo`.`groups` (`id`),
  CONSTRAINT `FKD8C8D8A5F25E5D5F`
    FOREIGN KEY (`extentityid`)
    REFERENCES `bamboo`.`external_entities` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.groups
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`groups` (
  `id` BIGINT(20) NOT NULL,
  `groupname` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.hibernate_unique_key
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`hibernate_unique_key` (
  `next_hi` INT(11) NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.imserver
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`imserver` (
  `IMSERVER_ID` BIGINT(20) NOT NULL,
  `TITLE` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `HOST` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `PORT` INT(11) NULL DEFAULT NULL,
  `USERNAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `RESOURCE_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PASSWORD` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SSL_REQUIRED` BIT(1) NOT NULL,
  `TLS_REQUIRED` BIT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`IMSERVER_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.local_members
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`local_members` (
  `userid` BIGINT(20) NOT NULL,
  `groupid` BIGINT(20) NOT NULL,
  PRIMARY KEY (`groupid`, `userid`),
  INDEX `FK6B8FB445CE2B3226` (`userid` ASC),
  CONSTRAINT `FK6B8FB445117D5FDA`
    FOREIGN KEY (`groupid`)
    REFERENCES `bamboo`.`groups` (`id`),
  CONSTRAINT `FK6B8FB445CE2B3226`
    FOREIGN KEY (`userid`)
    REFERENCES `bamboo`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.users
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`users` (
  `id` BIGINT(20) NOT NULL,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `password` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `email` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `fullname` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.notifications
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`notifications` (
  `NOTIFICATION_RULE_ID` BIGINT(20) NOT NULL,
  `CONDITION_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CONDITION_DATA` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `NOTIFICATION_SET` BIGINT(20) NULL DEFAULT NULL,
  `RECIPIENT` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `RECIPIENT_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`NOTIFICATION_RULE_ID`),
  INDEX `FK594ACC8707D72EE` (`NOTIFICATION_SET` ASC),
  CONSTRAINT `FK594ACC8707D72EE`
    FOREIGN KEY (`NOTIFICATION_SET`)
    REFERENCES `bamboo`.`notification_sets` (`NOTIFICATION_SET_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.os_propertyentry
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`os_propertyentry` (
  `entity_name` VARCHAR(125) CHARACTER SET 'utf8' NOT NULL,
  `entity_id` BIGINT(20) NOT NULL,
  `entity_key` VARCHAR(200) CHARACTER SET 'utf8' NOT NULL,
  `key_type` INT(11) NULL DEFAULT NULL,
  `boolean_val` BIT(1) NULL DEFAULT NULL,
  `double_val` DOUBLE NULL DEFAULT NULL,
  `string_val` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `text_val` MEDIUMTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `long_val` BIGINT(20) NULL DEFAULT NULL,
  `int_val` INT(11) NULL DEFAULT NULL,
  `date_val` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`entity_name`, `entity_id`, `entity_key`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.password_reset_token
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`password_reset_token` (
  `id` BIGINT(20) NOT NULL,
  `username` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `token` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `token_created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username` (`username` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.plan_dependencies
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`plan_dependencies` (
  `DEPENDENCY_ID` BIGINT(20) NOT NULL,
  `DEPENDENCY_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CHILD_PLAN_ID` BIGINT(20) NULL DEFAULT NULL,
  `PARENT_PLAN_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`DEPENDENCY_ID`),
  INDEX `plan_dependency_parent_id` (`PARENT_PLAN_ID` ASC),
  INDEX `plan_dependency_child_id` (`CHILD_PLAN_ID` ASC),
  CONSTRAINT `FKF971F67F6BDF075C`
    FOREIGN KEY (`PARENT_PLAN_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`),
  CONSTRAINT `FKF971F67F8D5E95AE`
    FOREIGN KEY (`CHILD_PLAN_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.plan_vcs_history
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`plan_vcs_history` (
  `PLAN_VCS_HISTORY_ID` BIGINT(20) NOT NULL,
  `PLAN_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `BUILD_NUMBER` INT(11) NOT NULL,
  `REVISION_KEY` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `VCS_LOCATION_ID` BIGINT(20) NOT NULL,
  `XML_CUSTOM_DATA` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`PLAN_VCS_HISTORY_ID`),
  INDEX `plan_vcs_plan_key_idx` (`PLAN_KEY` ASC),
  INDEX `plan_vcs_build_number_idx` (`BUILD_NUMBER` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.plan_vcs_location
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`plan_vcs_location` (
  `PLAN_VCS_LOCATION_ID` BIGINT(20) NOT NULL,
  `PLAN_ID` BIGINT(20) NULL DEFAULT NULL,
  `VCS_LOCATION_ID` BIGINT(20) NULL DEFAULT NULL,
  `LIST_POSITION` INT(11) NOT NULL,
  `BUILD_TRIGGER` BIT(1) NOT NULL,
  PRIMARY KEY (`PLAN_VCS_LOCATION_ID`),
  INDEX `plan_vcs_location_repo_id` (`VCS_LOCATION_ID` ASC),
  INDEX `plan_vcs_location_plan_id` (`PLAN_ID` ASC),
  CONSTRAINT `FKB7FED5E41D68B66C`
    FOREIGN KEY (`VCS_LOCATION_ID`)
    REFERENCES `bamboo`.`vcs_location` (`VCS_LOCATION_ID`),
  CONSTRAINT `FKB7FED5E4D5BADF1`
    FOREIGN KEY (`PLAN_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.queue
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`queue` (
  `QUEUE_ID` BIGINT(20) NOT NULL,
  `AGENT_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `TITLE` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `AGENT_DESCRIPTION` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  `ENABLED` BIT(1) NULL DEFAULT NULL,
  `CAPABILITY_SET` BIGINT(20) NULL DEFAULT NULL,
  `LAST_START_TIME` DATETIME NULL DEFAULT NULL,
  `LAST_STOP_TIME` DATETIME NULL DEFAULT NULL,
  `ELASTIC_INSTANCE_ID` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ELASTIC_IMAGE` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`QUEUE_ID`),
  INDEX `FK49D20F1A542349B` (`CAPABILITY_SET` ASC),
  INDEX `FK49D20F1581FB731` (`ELASTIC_IMAGE` ASC),
  CONSTRAINT `FK49D20F1581FB731`
    FOREIGN KEY (`ELASTIC_IMAGE`)
    REFERENCES `bamboo`.`elastic_image` (`ELASTIC_IMAGE_ID`),
  CONSTRAINT `FK49D20F1A542349B`
    FOREIGN KEY (`CAPABILITY_SET`)
    REFERENCES `bamboo`.`capability_set` (`CAPABILITY_SET_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.relevant_changesets
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`relevant_changesets` (
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NOT NULL,
  `REPOSITORY_CHANGESET_ID` BIGINT(20) NOT NULL,
  INDEX `rel_ch_brs_idx` (`BUILDRESULTSUMMARY_ID` ASC),
  INDEX `FK34A3E6E3CBADFCBD` (`REPOSITORY_CHANGESET_ID` ASC),
  CONSTRAINT `FK34A3E6E3A958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`),
  CONSTRAINT `FK34A3E6E3CBADFCBD`
    FOREIGN KEY (`REPOSITORY_CHANGESET_ID`)
    REFERENCES `bamboo`.`repository_changeset` (`REPOSITORY_CHANGESET_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.repository_changeset
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`repository_changeset` (
  `REPOSITORY_CHANGESET_ID` BIGINT(20) NOT NULL,
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NOT NULL,
  `VCS_LOCATION_ID` BIGINT(20) NOT NULL,
  `CHANGESET_ID` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SKIPPED_COMMITS_COUNT` BIGINT(20) NULL DEFAULT NULL,
  `LIST_POSITION` INT(11) NOT NULL,
  `BUILD_TRIGGER` BIT(1) NOT NULL,
  PRIMARY KEY (`REPOSITORY_CHANGESET_ID`),
  INDEX `repocommits_brs_idx` (`BUILDRESULTSUMMARY_ID` ASC),
  INDEX `repocommits_vcsloc_idx` (`VCS_LOCATION_ID` ASC),
  CONSTRAINT `FK4A5687DD1D68B66C`
    FOREIGN KEY (`VCS_LOCATION_ID`)
    REFERENCES `bamboo`.`vcs_location` (`VCS_LOCATION_ID`),
  CONSTRAINT `FK4A5687DDA958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.rememberme_token
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`rememberme_token` (
  `id` BIGINT(20) NOT NULL,
  `USERNAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CREATED` BIGINT(20) NOT NULL,
  `TOKEN` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `rmt_username_idx` (`USERNAME` ASC),
  INDEX `rmt_created_idx` (`CREATED` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.requirement
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`requirement` (
  `REQUIREMENT_ID` BIGINT(20) NOT NULL,
  `KEY_IDENTIFIER` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `REGEX_MATCH` BIT(1) NULL DEFAULT NULL,
  `READONLY_REQ` BIT(1) NULL DEFAULT NULL,
  `MATCH_VALUE` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLUGIN_MODULE_KEY` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `UNIQUE_IDENTIFIER` BIGINT(20) NULL DEFAULT NULL,
  `REQUIREMENT_SET` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`REQUIREMENT_ID`),
  INDEX `requirement_set_index` (`REQUIREMENT_SET` ASC),
  CONSTRAINT `FK95FC7023645E1626`
    FOREIGN KEY (`REQUIREMENT_SET`)
    REFERENCES `bamboo`.`requirement_set` (`REQUIREMENT_SET_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.test_case
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`test_case` (
  `TEST_CASE_ID` BIGINT(20) NOT NULL,
  `TEST_CLASS_ID` BIGINT(20) NULL DEFAULT NULL,
  `TEST_CASE_NAME` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `SUCCESSFUL_RUNS` INT(11) NULL DEFAULT NULL,
  `FAILED_RUNS` INT(11) NULL DEFAULT NULL,
  `AVG_DURATION` BIGINT(20) NULL DEFAULT NULL,
  `FIRST_BUILD_NUM` INT(11) NULL DEFAULT NULL,
  `LAST_BUILD_NUM` INT(11) NULL DEFAULT NULL,
  `QUARANTINING_USERNAME` VARCHAR(1000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `QUARANTINE_DATE` DATETIME NULL DEFAULT NULL,
  `LINKED_JIRA_ISSUE` VARCHAR(256) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`TEST_CASE_ID`),
  INDEX `testCase_testClassId_idx` (`TEST_CLASS_ID` ASC),
  CONSTRAINT `FK617BFABD2170166F`
    FOREIGN KEY (`TEST_CLASS_ID`)
    REFERENCES `bamboo`.`test_class` (`TEST_CLASS_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.test_class
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`test_class` (
  `TEST_CLASS_ID` BIGINT(20) NOT NULL,
  `TEST_CLASS_NAME` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLAN_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`TEST_CLASS_ID`),
  INDEX `testClass_planId_inx` (`PLAN_ID` ASC),
  CONSTRAINT `FKCE081B6BD5BADF1`
    FOREIGN KEY (`PLAN_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.test_case_result
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`test_case_result` (
  `TEST_CASE_RESULT_ID` BIGINT(20) NOT NULL,
  `TEST_CASE_ID` BIGINT(20) NULL DEFAULT NULL,
  `TEST_CLASS_RESULT_ID` BIGINT(20) NULL DEFAULT NULL,
  `DURATION` BIGINT(20) NULL DEFAULT NULL,
  `TEST_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `DELTA_STATE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `FAILING_SINCE` INT(11) NULL DEFAULT NULL,
  `QUARANTINED` BIT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`TEST_CASE_RESULT_ID`),
  INDEX `testCaseRes_deltaState_idx` (`DELTA_STATE` ASC),
  INDEX `testCaseRes_classResultId_idx` (`TEST_CLASS_RESULT_ID` ASC),
  INDEX `testCaseRes_caseId_idx` (`TEST_CASE_ID` ASC),
  INDEX `testCaseRes_state_idx` (`TEST_STATE` ASC),
  CONSTRAINT `FKD6859FDF1F5E6CA9`
    FOREIGN KEY (`TEST_CLASS_RESULT_ID`)
    REFERENCES `bamboo`.`test_class_result` (`TEST_CLASS_RESULT_ID`),
  CONSTRAINT `FKD6859FDF5AA11DDD`
    FOREIGN KEY (`TEST_CASE_ID`)
    REFERENCES `bamboo`.`test_case` (`TEST_CASE_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.test_class_result
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`test_class_result` (
  `TEST_CLASS_RESULT_ID` BIGINT(20) NOT NULL,
  `TEST_CLASS_ID` BIGINT(20) NULL DEFAULT NULL,
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NULL DEFAULT NULL,
  `DURATION` BIGINT(20) NULL DEFAULT NULL,
  `FAILED_COUNT` INT(11) NULL DEFAULT NULL,
  `SUCCESS_COUNT` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`TEST_CLASS_RESULT_ID`),
  INDEX `testClassRes_buildResSumId_idx` (`BUILDRESULTSUMMARY_ID` ASC),
  INDEX `testClassRes_testClassId_idx` (`TEST_CLASS_ID` ASC),
  CONSTRAINT `FK3521FF712170166F`
    FOREIGN KEY (`TEST_CLASS_ID`)
    REFERENCES `bamboo`.`test_class` (`TEST_CLASS_ID`),
  CONSTRAINT `FK3521FF71A958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.test_error
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`test_error` (
  `ERROR_ID` BIGINT(20) NOT NULL,
  `RESULT_ID` BIGINT(20) NULL DEFAULT NULL,
  `ERROR_CONTENT` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ERROR_ID`),
  INDEX `testError_caseResultId_idx` (`RESULT_ID` ASC),
  CONSTRAINT `FKCE2743FB39A4607D`
    FOREIGN KEY (`RESULT_ID`)
    REFERENCES `bamboo`.`test_case_result` (`TEST_CASE_RESULT_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.trusted_apps
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`trusted_apps` (
  `TRUSTED_APPS_ID` BIGINT(20) NOT NULL,
  `APP_ID` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `PUBLIC_KEY_CLOB` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `APP_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `TIMEOUT` BIGINT(20) NOT NULL,
  PRIMARY KEY (`TRUSTED_APPS_ID`),
  UNIQUE INDEX `APP_ID` (`APP_ID` ASC),
  INDEX `trust_apps_app_id_idx` (`APP_ID` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.trusted_apps_ips
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`trusted_apps_ips` (
  `IP_PATTERN_ID` BIGINT(20) NOT NULL,
  `IP_PATTERN` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  INDEX `FK257A172793E75E62` (`IP_PATTERN_ID` ASC),
  CONSTRAINT `FK257A172793E75E62`
    FOREIGN KEY (`IP_PATTERN_ID`)
    REFERENCES `bamboo`.`trusted_apps` (`TRUSTED_APPS_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.trusted_apps_urls
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`trusted_apps_urls` (
  `URL_PATTERN_ID` BIGINT(20) NOT NULL,
  `URL_PATTERN` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  INDEX `FK89CE4929D65FF53A` (`URL_PATTERN_ID` ASC),
  CONSTRAINT `FK89CE4929D65FF53A`
    FOREIGN KEY (`URL_PATTERN_ID`)
    REFERENCES `bamboo`.`trusted_apps` (`TRUSTED_APPS_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.user_comment
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`user_comment` (
  `COMMENT_ID` BIGINT(20) NOT NULL,
  `CONTENT` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `USER_NAME` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `CREATED_DATE` DATETIME NULL DEFAULT NULL,
  `UPDATED_DATE` DATETIME NULL DEFAULT NULL,
  `ENTITY_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`COMMENT_ID`),
  INDEX `comment_userName_idx` (`USER_NAME` ASC),
  INDEX `comment_entityId_idx` (`ENTITY_ID` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.variable_baseline_item
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`variable_baseline_item` (
  `VARIABLE_BASELINE_ITEM_ID` BIGINT(20) NOT NULL,
  `VARIABLE_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `VARIABLE_VALUE` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `VARIABLE_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `VARIABLE_CONTEXT_BASELINE_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`VARIABLE_BASELINE_ITEM_ID`),
  INDEX `var_base_ctx_type_idx` (`VARIABLE_TYPE` ASC),
  INDEX `var_item_baseline_idx` (`VARIABLE_CONTEXT_BASELINE_ID` ASC),
  CONSTRAINT `FKBD61044AE3B5B062`
    FOREIGN KEY (`VARIABLE_CONTEXT_BASELINE_ID`)
    REFERENCES `bamboo`.`variable_context_baseline` (`VARIABLE_CONTEXT_BASELINE_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.variable_context
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`variable_context` (
  `VARIABLE_CONTEXT_ID` BIGINT(20) NOT NULL,
  `VARIABLE_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `VARIABLE_VALUE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `VARIABLE_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`VARIABLE_CONTEXT_ID`),
  INDEX `var_ctx_type_idx` (`VARIABLE_TYPE` ASC),
  INDEX `var_context_result_idx` (`BUILDRESULTSUMMARY_ID` ASC),
  CONSTRAINT `FKB780EECCA958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.variable_definition
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`variable_definition` (
  `VARIABLE_DEFINITION_ID` BIGINT(20) NOT NULL,
  `VARIABLE_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `VARIABLE_VALUE` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `VARIABLE_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `PLAN_ID` BIGINT(20) NULL DEFAULT NULL,
  `ENVIRONMENT_ID` BIGINT(20) NULL DEFAULT NULL,
  `DEPLOYMENT_VERSION_ID` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`VARIABLE_DEFINITION_ID`),
  INDEX `variable_definition_type_idx` (`VARIABLE_TYPE` ASC),
  INDEX `var_def_plan_idx` (`PLAN_ID` ASC),
  INDEX `var_def_key_idx` (`VARIABLE_KEY` ASC),
  INDEX `var_def_dep_ver_idx` (`DEPLOYMENT_VERSION_ID` ASC),
  INDEX `var_def_env_idx` (`ENVIRONMENT_ID` ASC),
  CONSTRAINT `FK38458EF6D5BADF1`
    FOREIGN KEY (`PLAN_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.variable_substitution
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`variable_substitution` (
  `VARIABLE_SUBSTITUTION_ID` BIGINT(20) NOT NULL,
  `VARIABLE_KEY` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `VARIABLE_VALUE` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `VARIABLE_TYPE` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `BUILDRESULTSUMMARY_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`VARIABLE_SUBSTITUTION_ID`),
  INDEX `var_subst_result_idx` (`BUILDRESULTSUMMARY_ID` ASC),
  INDEX `var_subst_type_idx` (`VARIABLE_TYPE` ASC),
  CONSTRAINT `FK684A7BE0A958B29F`
    FOREIGN KEY (`BUILDRESULTSUMMARY_ID`)
    REFERENCES `bamboo`.`buildresultsummary` (`BUILDRESULTSUMMARY_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.variablestoautoincrement
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`variablestoautoincrement` (
  `VERSION_NAMING_SCHEME_ID` BIGINT(20) NOT NULL,
  `VARIABLE_NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`VERSION_NAMING_SCHEME_ID`, `VARIABLE_NAME`),
  CONSTRAINT `FKF61F414E928CEBA5`
    FOREIGN KEY (`VERSION_NAMING_SCHEME_ID`)
    REFERENCES `bamboo`.`deployment_version_naming` (`VERSION_NAMING_SCHEME_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table bamboo.vcs_branch
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `bamboo`.`vcs_branch` (
  `VCS_BRANCH_ID` BIGINT(20) NOT NULL,
  `NAME` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `CHAIN_ID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`VCS_BRANCH_ID`),
  INDEX `FKA348B0BB83C3FF79` (`CHAIN_ID` ASC),
  CONSTRAINT `FKA348B0BB83C3FF79`
    FOREIGN KEY (`CHAIN_ID`)
    REFERENCES `bamboo`.`build` (`BUILD_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;
SET FOREIGN_KEY_CHECKS = 1;
